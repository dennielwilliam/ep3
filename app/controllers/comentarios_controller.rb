class ComentariosController < InheritedResources::Base
  def create
    @prato = Prato.find(params[:prato_id])
    @comentario = @prato.comentarios.create(comentario_params)
    redirect_to prato_path(@prato)
  end

  def destroy
    @prato = Prato.find(params[:prato_id])
    @comentario = @prato.comentarios.find(params[:id])
    @comentario.destroy
    redirect_to prato_path(@prato)
  end

  def comentario_params
    params.require(:comentario).permit(:nome_usuario, :corpo, :prato_id)
  end
  
end
